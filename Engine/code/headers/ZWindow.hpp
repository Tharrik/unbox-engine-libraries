#pragma once

#include <string>
#include <internal/declarations.hpp>
#include <internal/error-codes.hpp>

namespace UnboxEngine {

	class ZWindow {

		using string = std::string;

	private:
		SDL_Window * window;
		SDL_GLContext glContext;

	public:
		ZWindow(const string & title, int width, int height, bool fullscreen = false);
		~ZWindow() = default;

		unsigned getWidth() const;
		unsigned getHeight() const;


	};

}
