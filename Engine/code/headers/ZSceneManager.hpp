#pragma once

#include <ZManager.hpp>
#include <ZScene.hpp>
#include <third-party/RapidXML/rapidxml.hpp>

namespace UnboxEngine {
	class ZSceneManager : ZManager<ZSceneManager> {
	public:
		static ZSceneManager& Instance() {
			static ZSceneManager instance;
			return instance;
		}

	protected:
		ZSceneManager() {}
		~ZSceneManager() {}

	public:
		static ZScene LoadSceneFromFile(string path);

	private:
		static rapidxml::xml_node<> * ReadXML(string path);

	public:
		// Inherited via ZManager
		virtual int StartUp() override;
		virtual int ShutDown() override;

	};
}
