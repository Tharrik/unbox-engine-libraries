/*
	Author:			Pablo del Fresno Herena
	Date:			21/12/2019
	Description:	Base class for unbox engine entities
*/

#pragma once

#include <algorithm>
#include <string>
#include <map>

#include <ZComponent.hpp>
#include <ZIDManager.hpp>

using namespace std;

namespace UnboxEngine {

	using ComponentMap = multimap<size_t, ZComponent&>;
	using ComponentMapType = pair<size_t, ZComponent&>;
	
	class ZEntity {

		using string = std::string;

		friend class ZScene;

	private:
		// Entity unique identifier. If the name already exits, the scene will deal with it.
		string name;
		ComponentMap components;

	public:
		ZEntity(string name) : name{ name } {}

		string GetName() const { return name; }

		void AddComponent(ZComponent& component) {
			components.insert(ComponentMapType(ID(component.GetType()), component));
		}
	};

}
