/*
	Author:			Pablo del Fresno
	Date:			23/12/2018
*/

#pragma once

#include <memory>

namespace UnboxEngine {

	enum TaskLevel {Load, Input, Update, LateUpdate, Render};
	enum TaskType {Persistent, Consumable};

	class ZTask {

	protected:
		size_t priority;
		TaskLevel level;
		TaskType type;

	public:
		ZTask(size_t priority, TaskLevel level = Update, TaskType type = Consumable) : priority{ priority }, level{ level }, type {type} { }

		virtual ~ZTask() {}

		virtual bool Initialize() = 0;
		virtual bool Finalize() = 0;
		virtual bool Step() = 0;

		bool operator < (const ZTask & other) const { return this->priority < other.priority; }
		bool operator > (const ZTask & other) const { return this->priority > other.priority; }
		bool operator == (const ZTask & other) const { return this->priority == other.priority; }

		size_t getPriority() const { return priority; }
		TaskLevel getLevel() const { return level; }
		TaskType getType() const { return type; }

	};

}
