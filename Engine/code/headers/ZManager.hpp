/*
	Author:			Pablo del Fresno Herena
	Date:			27/12/2018
	Description:	Generic class for managers/modules;
*/

#pragma once

namespace UnboxEngine {

	using namespace std;

	// CRTP for static methods override.
	template <typename DERIVED_TYPE>
	class ZManager {
	protected:
		size_t id = -1;

	public:

		static ZManager& Instance() { DERIVED_TYPE::Instance(); }

	protected:
		ZManager() {}

	public:

		virtual int StartUp() = 0;
		virtual int ShutDown() = 0;
	};

}
