/*
	Author:			Pablo del Fresno Herena
	Date:			23/12/2019
	Description:	Component base class for Unbox Engine
*/

#pragma once

#include <memory>

namespace UnboxEngine {

	// Forward declaration to break circular dependency
	class ZEntity;

	class ZComponent {
	private:
		std::shared_ptr<ZEntity> parent;

	public:
		ZComponent();
		virtual ~ZComponent() {}

		// Must return a string to identify the type
		virtual const string GetType() = 0;

	};

}
