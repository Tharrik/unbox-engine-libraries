#pragma once
#include <map>
#include <ZEntity.hpp>
#include <ZIDManager.hpp>

namespace UnboxEngine {

	using EntityMap = std::map<size_t, ZEntity>;
	using EntityMapType = std::pair<size_t, ZEntity>;

	class ZScene {

	private:
		EntityMap entities;

	public:
		ZScene() {}

		// Safe insert of entities in the entity map
		ZEntity& AddEntity(ZEntity& entity);
	};
}
