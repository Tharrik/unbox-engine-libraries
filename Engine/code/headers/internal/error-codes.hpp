#pragma once

namespace UnboxEngine{

#define ERROR_SDL_FAILED_INIT	1
#define ERROR_SDL_NO_WINDOW		2
#define ERROR_SDL_NO_CONTEXT	3

}
