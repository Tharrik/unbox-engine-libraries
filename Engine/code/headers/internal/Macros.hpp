#pragma once

#include <iostream>

using namespace std;

// Simple logging macros. TODO write log to a file.
#ifdef _DEBUG
#define UNBOX_LOG(X)		cout << "LOG: " << X << endl;
#else
#define UNBOX_LOG(X)
#endif
