/*
	Author:			Pablo del Fresno Herena
	Date:			10/01/2019
	Description:	Time manager for Unbox Engine
*/

#pragma once
#include <ZManager.hpp>
#include <chrono>

using namespace std::chrono;

namespace UnboxEngine {
	class ZTimeManager : ZManager<ZTimeManager> {

		using TimePoint = time_point<high_resolution_clock>;

		friend class ZCore;

	public:
		static ZTimeManager& Instance() {
			static ZTimeManager instance;
			return instance;
		}

	private:
		ZTimeManager() {}
		~ZTimeManager() {}

	private:
		static constexpr double nanosecondsInSecond = 1000000000;

	public:
		static double Delta() { return Instance().delta; }
		static double Elapsed();

	private:
		TimePoint engineStart = high_resolution_clock::now();
		TimePoint last = high_resolution_clock::now();
		int64_t deltans = 0;
		double delta = 0.0;

	public:
		int StartUp() override;
		int ShutDown() override;

	private:
		void UpdateDelta();
	};

}
