/*
	Author:			Pablo del Fresno Herena
	Date:			09/01/2019
	Description:	Is manager generates unique ids to be used 
*/

#pragma once

#include <ZManager.hpp>
#include <iostream>
#include <string>

namespace UnboxEngine {
	using string = std::string;

	class ZIDManager : public ZManager<ZIDManager> {


#pragma region Singleton
	public:
		static ZIDManager& Instance() {
			static ZIDManager instance;
			return instance;
		}

	private:
		ZIDManager() { id = 0; }

	public:
		~ZIDManager() {}

	public:
		static size_t generateID(string name) { return Instance().genID(name); }

#pragma endregion

	private:
		size_t genID(string name);

	public:
		// Inherited via ZManager
		int StartUp() override;
		int ShutDown() override;
	};

#define ID(X) ZIDManager::generateID(X)
}
