#pragma once

#include "ZCore.hpp"
#include <ZSceneManager.hpp>

namespace UnboxEngine
{
	/** /brief This static class is the entry point to the Unbox Engine.
	*
	*/
	class UNBOX {

	public:

		/** /brief Run Unbox Engine. Call after adding the game code.
		 *
		 */
		static void Run() { ZCore::Instance().Run(); }

		static void LoadScene(string path) { ZCore::Instance().LoadScene(path); }

		static void Exit() { ZCore::Instance().Exit(); }

	};

}
