/*
	Author:			Pablo del Fresno
	Creation:		21/12/2018
	Description:	UNBOX Engine task kernel
*/

#pragma once

#include <memory>
#include <map>
#include <iostream>
#include <list>

using namespace std;

#include "ZManager.hpp"
#include "ZTask.hpp"
#include "ZScene.hpp"

namespace UnboxEngine
{
	using TaskList = std::multimap<size_t, ZTask &>;
	using TaskListType = pair<size_t, ZTask &>;
	using TaskIterator = std::multimap<size_t, ZTask &>::iterator;

	class ZCore {

		friend class UNBOX;
		friend class ZTask;

#pragma region Singleton

	public:
		static ZCore & Instance() {
			static ZCore instance;
			return instance;
		}

	private:
		ZCore(){}

#pragma endregion

#pragma region Static calls

#pragma endregion

	private:
		bool exit = false;
		TaskList loadTasks, inputTasks, updateTasks, lateUpdateTasks, renderTasks;
		ZScene scene;

	public:
		void Run();
		void Exit();

		void AddTask(ZTask & task);
		void LoadScene(string path);

	private:
		int StartUp();
		int ShutDown();
		void ExecuteTaskList(TaskList& taskList);

	};


}
