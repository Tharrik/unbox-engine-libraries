
#include <SDL.h>
#include <OpenGL.hpp>

#include <ZWindow.hpp>

namespace UnboxEngine {

	ZWindow::ZWindow(const string & title, int width, int height, bool fullscreen)
	{
		window = nullptr;
		glContext = nullptr;

		std::atexit(SDL_Quit);

		if (SDL_InitSubSystem(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0) {
			SDL_Log("SDL Failed to initialize.");
			std::exit(ERROR_SDL_FAILED_INIT);
		}
		
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

		window = SDL_CreateWindow(
			title.c_str(),
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			width,
			height,
			SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
		);

		if (window == nullptr) {
			SDL_Log("SDL Failed to create a window.");
			std::exit(ERROR_SDL_NO_WINDOW);
		}

		glContext = SDL_GL_CreateContext(window);

		if (glContext == nullptr) {
			SDL_Log("SDL Failed to create a context.");
			std::exit(ERROR_SDL_NO_CONTEXT);
		}
		
		if (glt::initialize_opengl_extensions())
		{
			if (fullscreen)
			{
				SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
			}
		}

	}

}
