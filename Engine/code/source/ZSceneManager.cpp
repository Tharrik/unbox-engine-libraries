#include <ZSceneManager.hpp>
#include <fstream>
#include <vector>
#include <internal/Macros.hpp>

namespace UnboxEngine
{
	ZScene ZSceneManager::LoadSceneFromFile(string path)
	{
		ZScene scene{};

		rapidxml::xml_node<> * node = ReadXML(path);

		// TODO Load nodes to scene

		return scene;
	}

	int ZSceneManager::StartUp()
	{
		return 0;
	}

	int ZSceneManager::ShutDown()
	{
		return 0;
	}

	rapidxml::xml_node<> * ZSceneManager::ReadXML(string path)
	{
		fstream xmlFile{ path, fstream::in };
		// If the file is invalid returns an empty scene
		if (!xmlFile.good()) {
			UNBOX_LOG("Error. Invalid scene path.");
			exit(0);
		}

		// Load the content of the file to a vector
		vector<char>xmlFileContent;
		bool loaded = false;
		do {
			int character = xmlFile.get();
			if (character != -1) xmlFileContent.push_back((char)character);
			else loaded = true;
		} while (!loaded);

		// Write end of content so the parser knows where to stop
		xmlFileContent.push_back(0);

		// Parse to XML Doc
		rapidxml::xml_document<> document;
		document.parse<0>(xmlFileContent.data());

		// Get first node. If invalid, return an empty scene.
		rapidxml::xml_node<> * node = document.first_node();
		if (node == NULL) {
			UNBOX_LOG("Error. Invalid scene xml file.");
			exit(0);
		}

		return node;
	}
}
