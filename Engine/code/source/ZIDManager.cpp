#include "ZIDManager.hpp"
#include <iostream>

namespace UnboxEngine
{
	size_t UnboxEngine::ZIDManager::genID(string name)
	{
		// Fowler�Noll�Vo Hash Function
		if (name == "") return 0;
		// Prime and Seed for 64bit hashes
		const size_t prime = 0x100000001B3;
		size_t hash = 0xcbf29ce484222325;

		for (size_t i = 0; i < name.length(); ++i) {
			hash = ((unsigned char)(name[i]) ^ hash) * prime;
		}

		return hash;
	}

	int UnboxEngine::ZIDManager::StartUp()
	{
		return 0;
	}

	int UnboxEngine::ZIDManager::ShutDown()
	{
		return 0;
	}
}
