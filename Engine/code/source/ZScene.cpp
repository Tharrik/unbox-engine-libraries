#include "ZScene.hpp"

namespace UnboxEngine
{
	ZEntity & ZScene::AddEntity(ZEntity & entity)
	{
		int nameCount = 0;
		string name = entity.name;
		while (entities.find(ID(name)) != entities.end()) {
			name = entity.name + std::to_string(++nameCount);
		}
		entity.name = name;
		entities.insert(EntityMapType(ID(entity.name), entity));
		return entity;
	}
}
