#include "ZTimeManager.hpp"

namespace UnboxEngine
{
	double UnboxEngine::ZTimeManager::Elapsed()
	{
		TimePoint now = high_resolution_clock::now();
		int64_t elapsed = (duration_cast<nanoseconds>(now - Instance().engineStart)).count();
		return elapsed / nanosecondsInSecond;
	}

	int UnboxEngine::ZTimeManager::StartUp()
	{
		engineStart = high_resolution_clock::now();
		last = high_resolution_clock::now();
		deltans = 0;
		delta = 0.0;
		return 0;
	}

	int UnboxEngine::ZTimeManager::ShutDown()
	{
		return 0;
	}

	void UnboxEngine::ZTimeManager::UpdateDelta()
	{
		TimePoint now = high_resolution_clock::now();
		deltans = (duration_cast<nanoseconds>(now - last)).count();
		last = now;
		delta = deltans / nanosecondsInSecond;
	}

}