/*
	Author:		Pablo del Fresno
	Creation:	21/12/2018
*/

#pragma once

#include <SDL.h>
#include <ciso646>
#include <SDL_opengl.h>
#include <internal/error-codes.hpp>

#include <ZCore.hpp>
#include <ZWindow.hpp>
#include <ZTimeManager.hpp>
#include <ZSceneManager.hpp>

using namespace UnboxEngine;

void ZCore::Run()
{
	// Initialize kernel
	exit = false;

	// Initialize the engine
	StartUp();

	// Engine lives here
	while (!exit) {
		ExecuteTaskList(loadTasks);
		ExecuteTaskList(inputTasks);
		ExecuteTaskList(updateTasks);
		ExecuteTaskList(lateUpdateTasks);
		ExecuteTaskList(renderTasks);

		ZTimeManager::Instance().UpdateDelta();
	}

	// Shut down the engine
	ShutDown();
}

void UnboxEngine::ZCore::Exit()
{
	exit = true;
}

int ZCore::StartUp() {
	// Start all the modules/managers
	ZTimeManager::Instance().StartUp();
	return 0;
}

int ZCore::ShutDown() {
	// Shut down all the modules/managers
	return 0;
}

void UnboxEngine::ZCore::ExecuteTaskList(TaskList & taskList)
{
	for (TaskIterator it = taskList.begin(); it != taskList.end();) {
		if (exit) return;
		ZTask& task = it->second;
		task.Step();
		if (task.getType() == Consumable) taskList.erase(it++);
		else ++it;
	}
}

void UnboxEngine::ZCore::AddTask(ZTask & task)
{
	switch (task.getLevel()) {
		case TaskLevel::Load:
			loadTasks.insert(TaskListType(task.getPriority(), task));
			break;

		case TaskLevel::Input:
			inputTasks.insert(TaskListType(task.getPriority(), task));
			break;

		case TaskLevel::Update:
			updateTasks.insert(TaskListType(task.getPriority(), task));
			break;

		case TaskLevel::LateUpdate:
			lateUpdateTasks.insert(TaskListType(task.getPriority(), task));
			break;

		case TaskLevel::Render:
			renderTasks.insert(TaskListType(task.getPriority(), task));
			break;
	}
}

void UnboxEngine::ZCore::LoadScene(string path)
{
	scene = ZSceneManager::LoadSceneFromFile(path);
}

